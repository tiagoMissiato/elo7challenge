package br.com.tiagomissiato.elo7challenge;

import android.animation.Animator;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.List;

import br.com.tiagomissiato.elo7challenge.adapters.DrawerMenuAdapter;
import br.com.tiagomissiato.elo7challenge.bases.BaseActivity;
import br.com.tiagomissiato.elo7challenge.fragments.TransactionsFragment;
import br.com.tiagomissiato.elo7challenge.helpers.AnimationHelper;
import br.com.tiagomissiato.elo7challenge.helpers.AppHelper;
import br.com.tiagomissiato.elo7challenge.helpers.MenuHelper;
import br.com.tiagomissiato.elo7challenge.helpers.PreferenceHelper;
import br.com.tiagomissiato.elo7challenge.models.DrawerMenuItem;
import br.com.tiagomissiato.elo7challenge.views.FontFamilyTextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "uMAWLqEk6zh6i89GQQCwnWziE";
    private static final String TWITTER_SECRET = "IWm9HnXOK7O9hz1XIDkT2Na8OJv5ov1xHsJoIkkuYuHkmTLNb0";


    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Bind(R.id.navigation_drawer)
    RecyclerView mDrawerList;
    @Bind(R.id.drawer_menu_wrapper)
    RelativeLayout menuWrapper;
    @Bind(R.id.drawer_menu_user_name)
    FontFamilyTextView userName;
    @Bind(R.id.drawer_menu_company)
    FontFamilyTextView companyName;
    @Bind(R.id.reveal_view)
    public
    View revealView;

    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBarDrawerToggle mDrawerToggle;
    private Fragment mCurrentFragment;
    DrawerMenuAdapter adapter;
    List<DrawerMenuItem> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new TweetComposer());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        revealView.setVisibility(View.INVISIBLE);

        mLayoutManager = new LinearLayoutManager(this);
        mDrawerList.setLayoutManager(mLayoutManager);
        mDrawerList.setHasFixedSize(true);
        mDrawerList.setItemAnimator(new DefaultItemAnimator());

        //Set up toolbar
        configureToolbar();
        // Set up the drawer.
        configureDrawer();

        selectMenu(mItems.get(0), 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (revealView.getVisibility() == View.VISIBLE){
            int[] locationTo = PreferenceHelper.getCircularRevealFirst(this);
            startHideRevealEffect(locationTo[0], locationTo[1]);
        }
    }

    private void startHideRevealEffect(final int cx, final int cy) {

        if (cx != 0 && cy != 0) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                int color = PreferenceHelper.getCircularRevealColor(this);
                revealView.setBackgroundColor(color);
                AnimationHelper.hideRevealEffect(revealView, cx, cy, 1920);
                PreferenceHelper.storeCircularRevealColor(this, getResources().getColor(R.color.accent));
//                addTransaction.setVisibility(View.VISIBLE);
            }
        }
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
            }
        });
    }

    private void configureDrawer() {
        mItems = new ArrayList<>();

        mItems.add(MenuHelper.DRAWER_MENU_TRANSACTIONS, new DrawerMenuItem(getString(R.string.menu_transactions), MenuHelper.DRAWER_MENU_TRANSACTIONS, R.mipmap.ic_money_white, 0));

        adapter = new DrawerMenuAdapter(this, mItems, new DrawerMenuAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View v, int position) {
                selectMenu(mItems.get(position), position);
            }
        });
        mDrawerList.setAdapter(adapter);

        // Configure drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                mDrawerLayout.getViewTreeObserver().removeOnPreDrawListener(this);

                int width;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                    Display display = getWindowManager().getDefaultDisplay();
                    Point size = new Point();
                    display.getSize(size);
                    width = size.x;
                } else {
                    Display display = getWindowManager().getDefaultDisplay();
                    width = display.getWidth();
                }

                if (width > 0) {
                    int newWidth = width - AppHelper.getActionBarHeight(MainActivity.this, toolbar);

                    DrawerLayout.LayoutParams paramsW = (DrawerLayout.LayoutParams) menuWrapper.getLayoutParams();
                    paramsW.width = newWidth;
                    menuWrapper.setLayoutParams(paramsW);
                }
                return false;
            }
        });

        mDrawerToggle = new ActionBarDrawerToggle(
                this,  mDrawerLayout, toolbar,
                0, 0){

            public void onDrawerClosed(View view) {
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        ActionBar actionBar = getSupportActionBar();

        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        mDrawerToggle.syncState();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void selectMenu(DrawerMenuItem item, int position){

        if(item.getType() != DrawerMenuItem.ITEM_TYPE_SELECTED) {
            switch (item.getId()) {
                case MenuHelper.DRAWER_MENU_TRANSACTIONS:
                    setActionBarTitle(getString(R.string.menu_transactions));
                    handleNewFragment(TransactionsFragment.newInstance());
                    break;
            }

            updateMenuSelected(position);
        }

    }

    public void handleNewFragment(Fragment fragment){
        mCurrentFragment = fragment;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commitAllowingStateLoss();

        if (MenuHelper.isDrawerOpen(mDrawerLayout))
            mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    public void updateMenuSelected(int position){
        setMenuSelected(position);
        adapter.notifyDataSetChanged();
    }

    public void setMenuSelected(int position){
        for (DrawerMenuItem item : mItems) {
            if(item.getType() != DrawerMenuItem.ITEM_TYPE_DIVIDER)
                item.setType(DrawerMenuItem.ITEM_TYPE_NORMAL);
        }

        mItems.get(position).setType(DrawerMenuItem.ITEM_TYPE_SELECTED);
    }

    public void goToWithCircularReveal(Intent intent, View v){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation(this, Pair.create(v, "fab"));

            Animator.AnimatorListener revealAnimationListener = AnimationHelper.getRevealAnimatorListener(this, intent, transitionActivityOptions.toBundle());
            int[] locationFrom = PreferenceHelper.getCircularRevealFirst(this);
            revealView.setBackgroundColor(getResources().getColor(R.color.accent));
            AnimationHelper.showRevealEffect(revealView, locationFrom[0], locationFrom[1], revealAnimationListener);
        } else {
            startActivity(intent);
        }
    }

    @OnClick(R.id.drawer_item_settings)
    public void goToSettings(){
        startActivity(new Intent(this, SettingsActivity.class));
    }
}
