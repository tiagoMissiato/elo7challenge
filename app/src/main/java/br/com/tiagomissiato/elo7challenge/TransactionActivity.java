package br.com.tiagomissiato.elo7challenge;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import br.com.tiagomissiato.elo7challenge.bases.BaseActivity;
import br.com.tiagomissiato.elo7challenge.helpers.AnimationHelper;
import br.com.tiagomissiato.elo7challenge.helpers.CurrencyHelper;
import br.com.tiagomissiato.elo7challenge.helpers.DateHelper;
import br.com.tiagomissiato.elo7challenge.helpers.PreferenceHelper;
import br.com.tiagomissiato.elo7challenge.helpers.TwitterHelper;
import br.com.tiagomissiato.elo7challenge.models.Transaction;
import br.com.tiagomissiato.elo7challenge.views.AccountView;
import br.com.tiagomissiato.elo7challenge.views.DialogDatePicker;
import br.com.tiagomissiato.elo7challenge.views.FontFamilyEditText;
import br.com.tiagomissiato.elo7challenge.views.FontFamilyTextView;
import butterknife.Bind;
import butterknife.ButterKnife;

public class TransactionActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = TransactionActivity.class.getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.reveal_view)
    View revealView;

    @Bind(R.id.transaction_desc)
    FontFamilyEditText description;
    @Bind(R.id.account_from)
    AccountView accountFrom;
    @Bind(R.id.account_to)
    AccountView accountTo;
    @Bind(R.id.transaction_date)
    FontFamilyEditText transactionDate;
    @Bind(R.id.transaction_value)
    FontFamilyEditText value;
    @Bind(R.id.transaction_tax)
    FontFamilyEditText tax;
    @Bind(R.id.radio_type)
    RadioGroup radioType;
    @Bind(R.id.twitter_label)
    FontFamilyTextView twitterLabel;
    @Bind(R.id.share_twitter)
    SwitchCompat shareTwitter;
    @Bind(R.id.login_button)
    TwitterLoginButton loginButton;

    int taxType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int[] locationTo = PreferenceHelper.getCircularRevealSecond(this);
            startHideRevealEffect(locationTo[0], locationTo[1]);
        } else {
            revealView.setVisibility(View.GONE);
            overridePendingTransition(R.anim.right_slide_in, R.anim.none);
        }

        //Set up toolbar
        configureToolbar();

        transactionDate.setOnClickListener(this);
        transactionDate.setPreventKeyBoard(true);
        transactionDate.setOnClickListener(this);
        transactionDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    v.performClick();
            }
        });

        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterAuthToken authToken = result.data.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;

                PreferenceHelper.storeTwitterToken(TransactionActivity.this, token, secret);
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });

        if(TwitterHelper.getInstance(this).isLogged()) {
            loginButton.setVisibility(View.GONE);
            twitterLabel.setVisibility(View.VISIBLE);
            shareTwitter.setVisibility(View.VISIBLE);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            twitterLabel.setVisibility(View.GONE);
            shareTwitter.setVisibility(View.GONE);
        }

        transactionDate.setText(DateHelper.getInstance().getCurrentStrDate());

        radioType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                calculateNewTax();
            }
        });

        value.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if (!s.toString().equals(current)) {
                    value.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[R$,.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = CurrencyHelper.getInstance().getCurrencyFormatSymbol((int) parsed);

                    current = formatted;
                    value.setText(formatted);
                    value.setSelection(formatted.length());

                    value.addTextChangedListener(this);
                    calculateNewTax();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        value.setText("0");
        calculateNewTax();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode, resultCode, data);

        if(TwitterHelper.getInstance(this).isLogged()) {
            loginButton.setVisibility(View.GONE);
            twitterLabel.setVisibility(View.VISIBLE);
            shareTwitter.setVisibility(View.VISIBLE);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            twitterLabel.setVisibility(View.GONE);
            shareTwitter.setVisibility(View.GONE);
        }

    }

    private void calculateNewTax() {

        int checkedId = radioType.getCheckedRadioButtonId();

        int val = 0;
        try {
            String valueStr = value.getText().toString().replaceAll("[^\\d]", "");
            val = Integer.valueOf(valueStr);
        } catch (Exception ignored) {}

        switch (checkedId) {
            case R.id.tax_a:
                tax.setText(CurrencyHelper.getInstance().calculateTax(val, transactionDate.getText().toString(), CurrencyHelper.TAX_TYPE_A));
                taxType = CurrencyHelper.TAX_TYPE_A;
                break;
            case R.id.tax_b:
                tax.setText(CurrencyHelper.getInstance().calculateTax(val, transactionDate.getText().toString(), CurrencyHelper.TAX_TYPE_B));
                taxType = CurrencyHelper.TAX_TYPE_B;
                break;
            case R.id.tax_c:
                tax.setText(CurrencyHelper.getInstance().calculateTax(val, transactionDate.getText().toString(), CurrencyHelper.TAX_TYPE_C));
                taxType = CurrencyHelper.TAX_TYPE_C;
                break;
            case R.id.tax_d:
                tax.setText(CurrencyHelper.getInstance().calculateTax(val, transactionDate.getText().toString(), CurrencyHelper.TAX_TYPE_D));
                taxType = CurrencyHelper.TAX_TYPE_D;
                break;
        }
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        setActionBarTitle(getString(R.string.new_transaction));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void startHideRevealEffect(final int cx, final int cy) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            revealView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                @Override
                public void onViewAttachedToWindow(View v) {
                    revealView.setBackgroundColor(getResources().getColor(R.color.accent));
                    AnimationHelper.hideRevealEffect(revealView, cx, cy, 1920);
                }

                @Override
                public void onViewDetachedFromWindow(View v) {
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int[] locationTo = PreferenceHelper.getCircularRevealSecond(this);
            AnimationHelper.showRevealEffect(revealView, locationTo[0], locationTo[1], AnimationHelper.getRevealAnimatorListener(this));
        }
    }

    @Override
    public void finish() {
        super.finish();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            overridePendingTransition(R.anim.none, R.anim.right_slide_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_transaction, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save_transactino) {

            String valueStr = value.getText().toString().replaceAll("[R$,.]", "");
            String taxStr = tax.getText().toString().replaceAll("[R$,.]", "");
            int val = 0;
            int tax = 0;
            try {
                val = Integer.valueOf(valueStr);
            } catch (Exception ignored) {
            }
            try {
                tax = Integer.valueOf(taxStr);
            } catch (Exception ignored) {
            }

            Transaction transaction = new Transaction();
            transaction.description = description.getText().toString();
            transaction.value = val;
            transaction.taxValue = tax;
            transaction.createdAt = DateHelper.getInstance().getCurrentTimestamp();
            transaction.schedule = DateHelper.getInstance().getTimestampFromDate(transactionDate.getText().toString());
            transaction.accountFrom = accountFrom.getText();
            transaction.accountTo = accountFrom.getText();
            transaction.taxType = taxType;

            if(validate(transaction)) {
                PreferenceHelper.insertTransaction(this, transaction);

                if (shareTwitter.isChecked()) {
                    TwitterHelper.getInstance(this).postTwitter(transaction);
                }

                finish();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.transaction_date:
                DialogDatePicker startDatePicker = new DialogDatePicker();
                Bundle bndStart = new Bundle();
                bndStart.putString(DialogDatePicker.START_DATE, transactionDate.getText().toString());
                startDatePicker.setArguments(bndStart);

                startDatePicker.setOnDateSelected(new DialogDatePicker.OnDateSelected() {
                    @Override
                    public void dateSelected(String date) {
                        transactionDate.setText(date);
                        calculateNewTax();
                    }
                });
                startDatePicker.show(getSupportFragmentManager(), TAG);
                break;
        }
    }

    public boolean validate(Transaction transaction){

        if(transaction.description.length() == 0) {
            Snackbar.make(description, getString(R.string.descriptions_required), Snackbar.LENGTH_SHORT).show();
            return false;
        }

        if(accountFrom.getTextNoMask().length() == 0 || accountTo.getTextNoMask().length() == 0) {
            Snackbar.make(description, getString(R.string.descriptions_accounts_required), Snackbar.LENGTH_SHORT).show();
            return false;
        }

        if(transaction.value <= 0) {
            Snackbar.make(description, getString(R.string.value_error), Snackbar.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

}
