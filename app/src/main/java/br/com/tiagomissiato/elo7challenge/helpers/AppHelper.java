package br.com.tiagomissiato.elo7challenge.helpers;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.TypedValue;
import android.support.v7.widget.Toolbar;

/**
 * Created by tiagomissiato on 8/22/15.
 */
public class AppHelper {

    public static int getActionBarHeight(Context context, Toolbar toolbar) {
        int actionBarHeight = toolbar.getHeight();
        if(actionBarHeight <= 0) {
            TypedValue tv = new TypedValue();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
                        true))
                    actionBarHeight = TypedValue.complexToDimensionPixelSize(
                            tv.data, context.getResources().getDisplayMetrics());
            } else {
                TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                        new int[]{android.R.attr.actionBarSize});
                actionBarHeight = (int) styledAttributes.getDimension(0, 0);
                styledAttributes.recycle();


            }
        }
        return actionBarHeight;
    }

}
