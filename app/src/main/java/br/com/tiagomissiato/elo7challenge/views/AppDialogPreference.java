package br.com.tiagomissiato.elo7challenge.views;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.helpers.PreferenceHelper;


/**
 * Created by tiagomissiato on 8/15/15.
 */
public class AppDialogPreference extends DialogPreference {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AppDialogPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public AppDialogPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public AppDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AppDialogPreference(Context context) {
        super(context);
    }

    @Override
    protected View onCreateDialogView() {

        LinearLayout.LayoutParams LLParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        int padding = (int) getContext().getResources().getDimension(R.dimen.content_areas);
        LinearLayout layout = new LinearLayout(getContext());
        layout.setLayoutParams(LLParams);
        layout.setPadding(padding * 3, padding, padding * 3, padding);

        FontFamilyTextView message = new FontFamilyTextView(getContext());
        message.setText(getContext().getString(R.string.clear_transaction_message));

        layout.addView(message);

        return layout;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE){
            PreferenceHelper.clearTransactions(getContext());
        }
        super.onClick(dialog, which);
    }
}
