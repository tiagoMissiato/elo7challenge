package br.com.tiagomissiato.elo7challenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.List;

import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.models.DrawerMenuItem;
import br.com.tiagomissiato.elo7challenge.views.FontFamilyTextView;


/**
 * Created by trigoleto on 11/27/14.
 */
public class DrawerMenuAdapter extends RecyclerView.Adapter<DrawerMenuAdapter.ViewHolder> {

    Context mContext;
    private List<DrawerMenuItem> items;

    private OnItemClickListener onItemClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout wrapper;
        ImageView icon;
        FontFamilyTextView title;
        OnItemClickListener onItemClickListener;
        int type;

        public ViewHolder(View iteView, int type, OnItemClickListener onItemClickListener) {
            super(iteView);

            this.type = type;
            if(this.type != DrawerMenuItem.ITEM_TYPE_DIVIDER) {
                this.icon = (ImageView) iteView.findViewById(R.id.drawer_menu_icon);
                this.title = (FontFamilyTextView) iteView.findViewById(R.id.drawer_menu_title);
                this.wrapper = (RelativeLayout) iteView.findViewById(R.id.drawer_item_wrapper);
                this.onItemClickListener = onItemClickListener;
                this.wrapper.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            if(onItemClickListener != null && type != DrawerMenuItem.ITEM_TYPE_DIVIDER)
                onItemClickListener.onItemClicked(v, getPosition());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).getType();
    }

    public DrawerMenuAdapter(Context mContext, List<DrawerMenuItem> items, OnItemClickListener onItemClickListener) {
        this.items = items;
        this.onItemClickListener = onItemClickListener;
        this.mContext = mContext;

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View layout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_drawer_menu_item, null);
        if(viewType == DrawerMenuItem.ITEM_TYPE_DIVIDER){

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, (int) mContext.getResources().getDimension(R.dimen.drawer_menu_divider_height));
            int margins = (int) mContext.getResources().getDimension(R.dimen.drawer_menu_margin);
            params.setMargins(margins, margins, margins, margins);

            View v = new View(mContext);
            v.setLayoutParams(params);
            v.setBackgroundColor(mContext.getResources().getColor(R.color.menu_divider));
            layout = v;
        }

        return new ViewHolder(layout, viewType, onItemClickListener );
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        DrawerMenuItem item = this.items.get(position);

        if(getItemViewType(position) != DrawerMenuItem.ITEM_TYPE_DIVIDER) {
            viewHolder.icon.setImageResource(item.getIcon());
            viewHolder.title.setText(item.getTitle());

            if(item.getType() == DrawerMenuItem.ITEM_TYPE_SELECTED) {
                viewHolder.wrapper.setBackgroundResource(R.color.menu_selected);
                viewHolder.wrapper.setEnabled(false);
            }else {
                viewHolder.wrapper.setBackgroundResource(R.drawable.drawer_menu_item_feedback);
                viewHolder.wrapper.setEnabled(true);
            }
        }
    }

    public interface OnItemClickListener{
        void onItemClicked(View v, int position);
    }

}
