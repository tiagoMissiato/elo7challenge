package br.com.tiagomissiato.elo7challenge.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by tiagomissiato on 8/23/15.
 */
public class DateHelper {

    private static DateHelper sInstance;

    private Locale ptBR = new Locale("pt", "BR");
    private DateFormat formatter;
    Calendar calendar;

    public static DateHelper getInstance(){

        if(sInstance == null)
            sInstance = new DateHelper();

        sInstance.ptBR = new Locale("pt", "BR");
        sInstance.calendar = Calendar.getInstance();

        return sInstance;

    }

    public long getTimestampFromDate(String dateStr){

        Date date = null;
        formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            date = formatter.parse(dateStr.replaceAll("[/]", "-"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public String getDateFromTimestamp(long timestamp, String format){

        formatter = new SimpleDateFormat(format, ptBR);
        calendar.setTimeInMillis(timestamp);

        return formatter.format(calendar.getTime());
    }

    public int daysBetweenFromToday(String date){
        formatter = new SimpleDateFormat("dd/MM/yyyy", ptBR);

        int days = 0;
        try {
            Date today = getToday();
            Date transactionDate = formatter.parse(date);
            long diff = transactionDate.getTime() - today.getTime();
            days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return days;
    }

    public Date getToday(){
        calendar.setTimeInMillis(getCurrentTimestamp());
        return calendar.getTime();
    }


    public String getCurrentStrDate(){

        formatter = new SimpleDateFormat("dd/MM/yyyy", ptBR);
        calendar.setTimeInMillis(getCurrentTimestamp());

        return formatter.format(calendar.getTime());
    }

    public long getCurrentTimestamp(){
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTimeInMillis();
    }
}
