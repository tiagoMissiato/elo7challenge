package br.com.tiagomissiato.elo7challenge.helpers;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterApiException;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;

import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.models.Transaction;

/**
 * Created by tiagomissiato on 8/27/15.
 */
public class TwitterHelper {

    public static TwitterHelper sInstance;
    Context context;

    public static TwitterHelper getInstance(Context context){

        if(sInstance == null)
            sInstance = new TwitterHelper();

        sInstance.context = context;
        return sInstance;
    }

    public boolean isLogged(){
        return TwitterCore.getInstance().getSessionManager().getActiveSession() != null;
    }

    public void postTwitter(Transaction transaction){

        if(isLogged()){
            StringBuffer sb = new StringBuffer();
            sb.append(transaction.description)
                    .append(" - ")
                    .append(" C.c ")
                    .append(transaction.accountFrom)
                    .append(" -> ")
                    .append(transaction.accountTo)
                    .append(" - ")
                    .append(CurrencyHelper.getInstance().getCurrencyFormatSymbol(transaction.value))
                    .append(" - ")
                    .append(context.getString(R.string.tax_value_type_twitter, CurrencyHelper.getInstance().getCurrencyFormatSymbol(transaction.taxValue), CurrencyHelper.getInstance().getTaxType(transaction.taxType)))
                    .append(" em ")
                    .append(DateHelper.getInstance().getDateFromTimestamp(transaction.schedule, "dd/MM/yyyy"))
                    .append(" #elo7challenge");
            postTwitter(sb.toString());
        }

    }

    public void postTwitter(String text){
        if(isLogged()) {
            TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
            StatusesService statusesService = twitterApiClient.getStatusesService();
            statusesService.update(text, null, null, null, null, null, null, null, new Callback<Tweet>() {
                @Override
                public void success(Result<Tweet> tweetResult) {
                }

                @Override
                public void failure(TwitterException e) {
                    Snackbar.make(new View(context), ((TwitterApiException) e).getErrorMessage(), Snackbar.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void logout(){
        Twitter.getSessionManager().clearActiveSession();
        Twitter.logOut();
    }
}
