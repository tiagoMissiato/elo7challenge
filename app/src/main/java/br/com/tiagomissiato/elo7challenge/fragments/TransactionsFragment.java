package br.com.tiagomissiato.elo7challenge.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import java.util.List;

import br.com.tiagomissiato.elo7challenge.MainActivity;
import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.TransactionActivity;
import br.com.tiagomissiato.elo7challenge.adapters.TransactionListAdapter;
import br.com.tiagomissiato.elo7challenge.helpers.AnimationHelper;
import br.com.tiagomissiato.elo7challenge.helpers.PreferenceHelper;
import br.com.tiagomissiato.elo7challenge.models.Transaction;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransactionsFragment extends Fragment {

    @Bind(R.id.transactions_list)
    RecyclerView list;
    @Bind(R.id.fab)
    FloatingActionButton addTransaction;
    @Bind(R.id.no_transaction)
    LinearLayout noTransaction;

    MainActivity mActivity;
    LinearLayoutManager mLayoutManager;

    public static TransactionsFragment newInstance() {
        return new TransactionsFragment();
    }

    public TransactionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_transactions, container, false);
        ButterKnife.bind(this, layout);

        mActivity = (MainActivity) getActivity();

        addTransaction.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                addTransaction.getViewTreeObserver().removeOnPreDrawListener(this);
                getLocations();
                return false;
            }
        });

        mLayoutManager = new android.support.v7.widget.LinearLayoutManager(getActivity());
        list.setLayoutManager(mLayoutManager);
        list.setHasFixedSize(true);
        list.setItemAnimator(new DefaultItemAnimator());

        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();

        populateTransactionList();
    }

    public void populateTransactionList(){
        List<Transaction> items = PreferenceHelper.getTransactions(getActivity());
        if(items.size() > 0) {
            noTransaction.setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
            TransactionListAdapter adapter = new TransactionListAdapter(getActivity(), items);
            list.setAdapter(adapter);
        } else {
            noTransaction.setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
        }
    }

    public void getLocations(){

        int [] locationFirst = new int[2];

        addTransaction.getLocationOnScreen(locationFirst);

        int cx = (locationFirst[0] + (addTransaction.getWidth() / 2));
        int cy = locationFirst[1] + (AnimationHelper.getStatusBarHeight(getActivity()) / 2);

        PreferenceHelper.storeCircularRevealFirst(getActivity(), cx, cy);
        PreferenceHelper.storeCircularRevealSecond(getActivity(), 0, 0);
    }

    @OnClick(R.id.fab)
    public void goToTransactionActivity(){
        Intent i = new Intent(getActivity(), TransactionActivity.class);

        mActivity.goToWithCircularReveal(i, addTransaction);
    }

}
