package br.com.tiagomissiato.elo7challenge.bases;

import android.app.Application;

/**
 * Created by tiagomissiato on 8/22/15.
 */
public class BaseApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
