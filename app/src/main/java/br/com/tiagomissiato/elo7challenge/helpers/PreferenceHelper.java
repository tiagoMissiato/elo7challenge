package br.com.tiagomissiato.elo7challenge.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import br.com.tiagomissiato.elo7challenge.models.Transaction;

/**
 * Created by tiagomissiato on 8/22/15.
 */
public class PreferenceHelper {

    private final static String APP_PREFERENCE = "preference";
    private final static String PREFERENCE_CIRCULAR_REVEAL_FIRST = "preference.circular.reveal.first";
    private final static String PREFERENCE_CIRCULAR_REVEAL_SECOND = "preference.circular.reveal.second";
    private final static String PREFERENCE_CIRCULAR_REVEAL_COLOR = "preference.circular.reveal.color";
    private final static String PREFERENCE_LOCATION_CX = ".cx";
    private final static String PREFERENCE_LOCATION_CY = ".cy";
    private final static String PREFERENCE_TRANSACTIONS = "preference.transaction";
    private final static String PREFERENCE_TWITTER_TOKEN = "preference.twitter.token";
    private final static String PREFERENCE_TWITTER_SECRET = "preference.twitter.secret";

    public static void storeTwitterToken(Context mContext, String token, String secret){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREFERENCE_TWITTER_TOKEN, token);
        editor.putString(PREFERENCE_TWITTER_SECRET, secret);
        editor.apply();
    }

    public String getTwitterToken(Context mContext){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PREFERENCE_TWITTER_TOKEN, null);
    }

    public String getTwitterSecret(Context mContext){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        return sharedpreferences.getString(PREFERENCE_TWITTER_SECRET, null);
    }

    public static void storeCircularRevealColor(Context mContext, int rs){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(PREFERENCE_CIRCULAR_REVEAL_COLOR, rs);
        editor.apply();
    }

    public static int getCircularRevealColor(Context mContext){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        return sharedpreferences.getInt(PREFERENCE_CIRCULAR_REVEAL_COLOR, 0);
    }

    public static void storeCircularRevealFirst(Context mContext, int cx, int cy){
        int[] toStore = new int[2];
        toStore[0] = cx;
        toStore[1] = cy;

        storeCircularReveal(mContext, PREFERENCE_CIRCULAR_REVEAL_FIRST, toStore);
    }

    public static void storeCircularRevealSecond(Context mContext, int cx, int cy){
        int[] toStore = new int[2];
        toStore[0] = cx;
        toStore[1] = cy;

        storeCircularReveal(mContext, PREFERENCE_CIRCULAR_REVEAL_SECOND, toStore);
    }

    public static int[] getCircularRevealFirst(Context mContext){
        return getCircularRevealLocation(mContext, PREFERENCE_CIRCULAR_REVEAL_FIRST);
    }

    public static int[] getCircularRevealSecond(Context mContext){
        return getCircularRevealLocation(mContext, PREFERENCE_CIRCULAR_REVEAL_SECOND);
    }

    public static void storeCircularReveal(Context mContext, String preference, int[] location){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(preference + PREFERENCE_LOCATION_CX, location[0]);
        editor.putInt(preference + PREFERENCE_LOCATION_CY, location[1]);
        editor.apply();
    }

    public static int[] getCircularRevealLocation(Context mContext, String preference){
        int[] toReturn = new int[2];

        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        toReturn[0] = sharedpreferences.getInt(preference + PREFERENCE_LOCATION_CX, 0);
        toReturn[1] = sharedpreferences.getInt(preference + PREFERENCE_LOCATION_CY, 0);

        return toReturn;
    }

    public static void insertTransaction(Context mContext, Transaction transaction){

        List<Transaction> transactions = getTransactions(mContext);
        if(transactions == null)
            transactions = new ArrayList<>();

        transactions.add(transaction);

        Gson gson = new Gson();

        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREFERENCE_TRANSACTIONS, transaction != null ? gson.toJson(transactions) : null);
        editor.apply();

    }

    public static List<Transaction> getTransactions(Context mContext){
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        String usersJson = sharedpreferences.getString(PREFERENCE_TRANSACTIONS, null);
        List<Transaction> toReturn = new ArrayList<>();

        if(usersJson != null) {
            Gson gson = new Gson();
            toReturn = gson.fromJson(usersJson, new TypeToken<List<Transaction>>(){}.getType());
        }
        return toReturn;
    }

    public static void clearTransactions(Context mContext) {
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(APP_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PREFERENCE_TRANSACTIONS, null);
        editor.apply();
    }
}
