package br.com.tiagomissiato.elo7challenge.helpers;

import android.support.annotation.IntDef;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by trigoleto on 7/1/15.
 * t.m.rigoleto@gmail.com
 */
public class CurrencyHelper {

    public static final int TAX_TYPE_A = 0;
    public static final int TAX_TYPE_B = 1;
    public static final int TAX_TYPE_C = 2;
    public static final int TAX_TYPE_D = 3;

    @IntDef({TAX_TYPE_A, TAX_TYPE_B, TAX_TYPE_C, TAX_TYPE_D})
    public @interface TaxType {}

    private static CurrencyHelper sInstance;

    Locale ptBR;
    NumberFormat format;

    public static CurrencyHelper getInstance(){

        if(sInstance == null)
            sInstance = new CurrencyHelper();

        sInstance.ptBR = new Locale("pt", "BR");
        sInstance.format = NumberFormat.getCurrencyInstance(sInstance.ptBR);

        return sInstance;

    }

    public String getCurrencyFormatSymbol(int number){
        return format.format((number / 100.00));
    }

    public String getCurrencyFormatSymbol(double number){
        return format.format((number));
    }

    public String calculateTax(int value, String date, @TaxType int type){

        double val = value / 100.00;
        String strVal = "";
        switch (type){
            case TAX_TYPE_A:
                val = 2 + addPercentage(val, 2.0);
                strVal = getCurrencyFormatSymbol(val);
                break;
            case TAX_TYPE_B:
                val = DateHelper.getInstance().daysBetweenFromToday(date) <= 30 ? 10 : 8;
                strVal = getCurrencyFormatSymbol(val);
                break;
            case TAX_TYPE_C:
                int days = DateHelper.getInstance().daysBetweenFromToday(date);
                if(days > 30)
                    val = addPercentage(val, 1.2);
                else if(days > 25 && days <= 30)
                    val = addPercentage(val, 2.1);
                else if(days > 20 && days <= 25)
                    val = addPercentage(val, 4.3);
                else if(days > 15 && days <= 20)
                    val = addPercentage(val, 5.4);
                else if(days > 10 && days <= 15)
                    val = addPercentage(val, 6.7);
                else if(days > 5 && days <= 10)
                    val = addPercentage(val, 7.4);
                else
                    val = addPercentage(val, 8.3);

                strVal = getCurrencyFormatSymbol(val);
                break;
            case TAX_TYPE_D:
                if(value <= 25000)
                    strVal = calculateTax(value, date, TAX_TYPE_A);
                else if(value > 25000 && value <= 120000)
                    strVal = calculateTax(value, date, TAX_TYPE_B);
                else
                    strVal = calculateTax(value, date, TAX_TYPE_C);
                break;
        }

        return strVal;
    }

    public double addPercentage(double val, double percentage){
        return ((val * (percentage / 100.00)));
    }

    public String getTaxType(int type){
        String typeStr = "A";
        switch (type){
            case TAX_TYPE_B:
                typeStr = "B";
                break;
            case TAX_TYPE_C:
                typeStr = "C";
                break;
            case TAX_TYPE_D:
                typeStr = "D";
                break;
        }
        return typeStr;
    }
}
