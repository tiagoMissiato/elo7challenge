package br.com.tiagomissiato.elo7challenge.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.tiagomissiato.elo7challenge.R;

/**
 * Created by tiagomissiato on 8/22/15.
 */
public class AccountView extends LinearLayout implements View.OnFocusChangeListener{

    private static final String TAG = AccountView.class.getSimpleName();

    Context mContext;

    //the space between the fixed character
    private float charSpace;

    //space between the delimiter
    private float delimiterSpace;

    //color to the current position
    private int selectedItemColor;

    //color to the delimiter not typed yet
    private int unselectedItemColor;

    //specify if the typed character must be forced to uppercase
    private boolean toUpperCase;

    private int textColor;

    //hold the view to replace by correct current view
    List<View> views = new ArrayList<>();

    //hold the fixed character to not be replaced
    HashMap<Integer, String> fixedPositions = new HashMap<>();

    private float fontSize;
    private String fontFamily;
    private String mask;
    private Typeface typeface;
    private int characterContainerWidht;
    private int characterContainerHeight;
    private int delimiterLineHeight;
    private int currentPosition = 0, maskTotalChar = 0;

    public AccountView(Context context) {
        super(context);
        this.mContext = context;

        if(!isInEditMode())
            init(null);
    }

    public AccountView(Context context, String mask) {
        super(context);
        this.mContext = context;
        this.mask = mask;

        if(!isInEditMode())
            init(null);
    }

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;

        if(!isInEditMode())
            init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public AccountView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if(!isInEditMode())
            init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AccountView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;

        if(!isInEditMode())
            init(attrs);
    }

    public void init(AttributeSet attrs){
        Log.i(TAG, "init");

        /**
         * Set main configuration
         */
        setOrientation(HORIZONTAL);
        setGravity(Gravity.BOTTOM);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setOnFocusChangeListener(this);

        characterContainerWidht = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 15, getResources().getDisplayMetrics());
        characterContainerHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, getResources().getDisplayMetrics());
        delimiterLineHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics());

        /**
         * Get theme colors
         */
        int primaryColor = Color.BLACK;
        int accentColor = Color.WHITE;


        int defaultPrimaryColor;
        int defaultAccentColor;
        TypedValue primaryColorTypedValue = new TypedValue();
        TypedValue accentColorTypedValue = new TypedValue();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mContext.getTheme().resolveAttribute(android.R.attr.colorPrimary, primaryColorTypedValue, true);
                mContext.getTheme().resolveAttribute(android.R.attr.colorAccent, accentColorTypedValue, true);
                defaultPrimaryColor = primaryColorTypedValue.data;
                defaultAccentColor = accentColorTypedValue.data;
            } else {
                throw new RuntimeException("SDK_INT less than LOLLIPOP");
            }
        } catch (Exception e) {
            try {
                int colorPrimaryId = getResources().getIdentifier("colorPrimary", "attr", getContext().getPackageName());
                int colorAccentId = getResources().getIdentifier("colorAccent", "attr", getContext().getPackageName());
                if (colorPrimaryId != 0) {
                    mContext.getTheme().resolveAttribute(colorPrimaryId, primaryColorTypedValue, true);
                    defaultPrimaryColor = primaryColorTypedValue.data;
                } else {
                    defaultPrimaryColor = primaryColor;
                }

                if(colorAccentId != 0){
                    mContext.getTheme().resolveAttribute(colorPrimaryId, accentColorTypedValue, true);
                    defaultAccentColor = accentColorTypedValue.data;
                } else {
                    defaultAccentColor = accentColor;
                }
            } catch (Exception e1) {
                defaultPrimaryColor = primaryColor;
                defaultAccentColor = accentColor;
            }
        }

        float defaultFontSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 18, getResources().getDisplayMetrics());
        float defaultCharSpace = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics());
        float defaultDelimiterSpace = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 5, getResources().getDisplayMetrics());

        /**
         * Set custom values
         */
        TypedArray typedArray = mContext.obtainStyledAttributes(attrs, R.styleable.AccountView);
        fontFamily = typedArray.getString(R.styleable.AccountView_fontFamily);
        fontSize = typedArray.getDimensionPixelSize(R.styleable.AccountView_fontSize, (int) defaultFontSize);
        textColor = typedArray.getColor(R.styleable.AccountView_textColor, defaultPrimaryColor);
        mask = typedArray.getString(R.styleable.AccountView_mask);
        toUpperCase = typedArray.getBoolean(R.styleable.AccountView_toUppercase, true);
        charSpace = typedArray.getDimension(R.styleable.AccountView_charSpace, defaultCharSpace);
        delimiterSpace = typedArray.getDimension(R.styleable.AccountView_delimiterSpace, defaultDelimiterSpace);
        selectedItemColor = typedArray.getColor(R.styleable.AccountView_selectedItemColor, defaultAccentColor);
        unselectedItemColor = typedArray.getColor(R.styleable.AccountView_unselectedItemColor, defaultPrimaryColor);
        typedArray.recycle();

        /**
         * Set type face
         */
        if(this.fontFamily != null){
            try {
                typeface = Typeface.createFromAsset(mContext.getAssets(), this.fontFamily);
            } catch (Exception e) {}

        }

        maskTotalChar = mask.length();

        //Create delimiters and get fixes character
        for(int i = 0; i < maskTotalChar; i++){
            char c = mask.charAt(i);

            if(c == '#') {
                addView(createDelimiter());
            }else {
                fixedPositions.put(i, Character.toString(c));
                addView(createFixedView(c));
            }
        }
    }

    public View createDelimiter(){

        RelativeLayout relativeLayout = new RelativeLayout(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = characterContainerHeight;
        params.width = characterContainerWidht;
        params.setMargins(0,0,(int) delimiterSpace, 0);

        relativeLayout.setLayoutParams(params);
        relativeLayout.setGravity(Gravity.BOTTOM);

        relativeLayout.addView(getLineView());
        views.add(relativeLayout);

        return relativeLayout;

    }

    private View getLineView(){
        LinearLayout.LayoutParams paramsLine = new LinearLayout.LayoutParams(characterContainerWidht, ViewGroup.LayoutParams.WRAP_CONTENT);
        paramsLine.height = delimiterLineHeight;

        View v = new View(mContext);
        v.setLayoutParams(paramsLine);
        v.setBackgroundColor(unselectedItemColor);

        return v;
    }

    public View createFixedView(char character){

        RelativeLayout relativeLayout = new RelativeLayout(mContext);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.height = characterContainerHeight;
        params.width = characterContainerWidht;
        params.setMargins(0, 0, (int) charSpace, 0);

        relativeLayout.setLayoutParams(params);
        relativeLayout.setGravity(Gravity.BOTTOM);

        relativeLayout.addView(getTextViewCharacter(character));
        views.add(relativeLayout);

        return relativeLayout;
    }

    private TextView getTextViewCharacter(char c){

        TextView textView = new TextView(mContext);
        textView.setText(getStrFromChar(c));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, fontSize);
        textView.setGravity(Gravity.CENTER);
        textView.setTextColor(textColor);
        if(typeface != null)
            textView.setTypeface(typeface);

        return textView;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(event.getAction() == MotionEvent.ACTION_DOWN){
            requestFocus();
            showKeyboard(mContext, this);
        }

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        if(keyCode == KeyEvent.KEYCODE_DEL && currentPosition > 0){
            currentPosition--;
            prevPosition();
            if(currentPosition >= 0) {
                RelativeLayout rV = (RelativeLayout) views.get(currentPosition);
                rV.removeViewAt(0);
                rV.addView(getLineView(), 0);
                moveToPrevSelection();
            }
        } else if(keyCode != KeyEvent.KEYCODE_SHIFT_LEFT && keyCode != KeyEvent.KEYCODE_DEL && currentPosition < maskTotalChar){

            nextPosition();
            if(currentPosition < maskTotalChar) {
                char pressedKey = (char) event.getUnicodeChar();

                RelativeLayout rV = (RelativeLayout) views.get(currentPosition);
                rV.removeViewAt(0);
                rV.addView(getTextViewCharacter(pressedKey), 0);

                currentPosition++;
                moveToNextSelection();
            }
        }

        fixCurrentPositionIfNecessary();
        return super.onKeyUp(keyCode, event);
    }

    private void prevPosition(){
        while(fixedPositions.get(currentPosition) != null){
            currentPosition--;
        }
    }

    private void nextPosition(){
        while(fixedPositions.get(currentPosition) != null){
            currentPosition++;
        }
    }

    private void fixCurrentPositionIfNecessary(){
        if(currentPosition < 0)
            currentPosition = 0;

        if(currentPosition > maskTotalChar)
            currentPosition = maskTotalChar;
    }

    public String getStrFromChar(char c){
        if(toUpperCase)
            return Character.toString(c).toUpperCase();
        else
            return Character.toString(c);
    }

    private void moveToNextSelection(){
        nextPosition();

        setSelected(currentPosition);
    }

    private void moveToPrevSelection(){

        prevPosition();

        if(currentPosition >= 0) {
            setSelected(currentPosition);
        }

        if(currentPosition < maskTotalChar -1) {

            int auxPos = currentPosition + 1;

            while(fixedPositions.get(auxPos) != null && auxPos <= maskTotalChar){
                auxPos++;
            }

            if(auxPos <= maskTotalChar -1) {
                setUnelected(auxPos);
            }
        }

    }

    private void setSelected(int position){
        if(position < maskTotalChar) {
            RelativeLayout rV = (RelativeLayout) views.get(position);

            if (position == 0) {
                rV.getChildAt(0).setBackgroundColor(selectedItemColor);
            } else if (position < maskTotalChar) {
                rV.getChildAt(0).setBackgroundColor(selectedItemColor);
            }

        }
    }

    private void setUnelected(int position){
        if(position >= 0 && position <= maskTotalChar -1) {
            RelativeLayout uV = (RelativeLayout) views.get(position);
            uV.getChildAt(0).setBackgroundColor(unselectedItemColor);
        }
    }

    public void showKeyboard(Context context, View view) {
        InputMethodManager manager = (InputMethodManager)context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.showSoftInput(view, 0);
    }

    public void hideKeyboard(Context context, View view) {
        if (view != null) {
            InputMethodManager manager = (InputMethodManager)context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public String getText(){
        String text = "";
        for (View v : views){
            View textView = ((RelativeLayout)v).getChildAt(0);
            if(textView instanceof TextView)
                text += ((TextView) textView).getText().toString();

        }
        return text;
    }

    public String getTextNoMask(){
        String text = "";
        int index = 0;
        for (View v : views){
            View textView = ((RelativeLayout)v).getChildAt(0);
            if(textView instanceof TextView && fixedPositions.get(index) == null)
                text += ((TextView) textView).getText().toString();

            index++;
        }
        return text;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus) {
            hideKeyboard(mContext, this);
            setUnelected(currentPosition);
        } else {
            moveToNextSelection();
        }
    }
}
