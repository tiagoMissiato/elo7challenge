package br.com.tiagomissiato.elo7challenge.views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.helpers.DateHelper;

/**
 * Created by tiagomissiato on 8/23/15.
 */
public class DialogDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public final static String START_DATE = "start.date";

    Button btnDate;

    public void setOnDateSelected(OnDateSelected onDateSelected) {
        this.onDateSelected = onDateSelected;
    }

    OnDateSelected onDateSelected;
    DatePickerDialog dialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        String startDate = null;
        if(getArguments() != null)
            startDate = getArguments().getString(START_DATE);

        if(startDate != null){
            String[] dateArray = startDate.split("/");
            if(dateArray.length == 3){
                dialog = new DatePickerDialog(getActivity(), this, Integer.valueOf(dateArray[2]), Integer.valueOf(dateArray[1]) - 1, Integer.valueOf(dateArray[0]));
            } else {
                dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            }

        } else {
            dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            //-1000 because you can't set min date to exactly now
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        }
        return dialog;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {

        //check date for api < 11
        if (DateHelper.getInstance().getTimestampFromDate(day + "/" + month + "/" + year) < DateHelper.getInstance().getCurrentTimestamp())
            Toast.makeText(getActivity(), getString(R.string.invalid_date_schedule, DateHelper.getInstance().getCurrentStrDate()), Toast.LENGTH_SHORT).show();
        else
            setDate(year,month,day);
    }

    public void setDate(int year, int month, int day){

        String dayP = Integer.toString(day);
        String monthP = Integer.toString(month + 1);

        String str_date = dayP+"-"+monthP+"-"+Integer.toString(year);
        String dateReturn = dayP+"/"+monthP+"/"+Integer.toString(year);
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", new Locale("pt", "BR"));
        try {
            Date date = formatter.parse(str_date);
            dateReturn = DateHelper.getInstance().getDateFromTimestamp(date.getTime(), "dd/MM/yyyy");
        } catch (ParseException e) {}

        if(onDateSelected != null)
            onDateSelected.dateSelected(dateReturn);

    }

    public interface OnDateSelected {
        public abstract void dateSelected(String date);
    }

}