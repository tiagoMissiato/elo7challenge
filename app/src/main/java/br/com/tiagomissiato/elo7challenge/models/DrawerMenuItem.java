package br.com.tiagomissiato.elo7challenge.models;

import java.io.Serializable;

/**
 * Created by trigoleto on 11/27/14.
 */
public class DrawerMenuItem implements Serializable {

    public static final int ITEM_TYPE_NORMAL = 0;
    public static final int ITEM_TYPE_SELECTED = 1;
    public static final int ITEM_TYPE_DIVIDER = 2;


    private String title;
    private int id;
    private int icon;
    private int count;
    private int type;

    public DrawerMenuItem(String title, int id, int icon, int count) {
        this.title = title;
        this.id = id;
        this.icon = icon;
        this.count = count;
        this.type = DrawerMenuItem.ITEM_TYPE_NORMAL;
    }

    public DrawerMenuItem(String title, int id, int icon, int count, int type) {
        this.title = title;
        this.id = id;
        this.icon = icon;
        this.count = count;
        this.type = type;
    }

    public String getTitle() {return title;}

    public int getId() {return id;}

    public int getIcon() {return icon;}

    public int getType() {return type;}

    public void setId(int id) {
        this.id = id;
    }

    public void setIcon(int icon) {this.icon = icon;}

    public int getCount() {return count;}

    public void setCount(int count) {this.count = count;}

    public void setTitle(String title) {this.title = title;}

    public void setType(int type) {this.type = type;}
}
