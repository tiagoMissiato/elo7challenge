package br.com.tiagomissiato.elo7challenge.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import br.com.tiagomissiato.elo7challenge.R;

/**
 * Created by tiagomissiato on 8/23/15.
 */
public class FontFamilyEditText extends EditText {

    Context context;
    String fontFamily = null;

    boolean preventKeyBoard = false;

    public FontFamilyEditText(Context context) {
        super(context);
        this.context = context;

        if(!isInEditMode())
            init();
    }

    public FontFamilyEditText(Context context, String fontFamily) {
        super(context);
        this.context = context;
        this.fontFamily = fontFamily;

        if(!isInEditMode())
            init();
    }

    public FontFamilyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;

        TypedArray a=context.obtainStyledAttributes(attrs, R.styleable.FontFamilyEditText);
        this.fontFamily = a.getString(R.styleable.FontFamilyEditText_fmetFontFamily);
        a.recycle();

        if(!isInEditMode())
            init();
    }

    public FontFamilyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray a=context.obtainStyledAttributes(attrs, R.styleable.FontFamilyEditText);
        this.fontFamily = a.getString(R.styleable.FontFamilyEditText_fmetFontFamily);
        a.recycle();

        if(!isInEditMode())
            init();
    }

    void init() {

        if(this.fontFamily != null){
            try {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), this.fontFamily);
                this.setTypeface(myTypeface);
            } catch (Exception e) {}

        }

    }

    @Override
    public boolean onCheckIsTextEditor() {
        if(!preventKeyBoard)
            return super.onCheckIsTextEditor();
        else
            return false;
    }

    public void setPreventKeyBoard(boolean preventKeyBoard) {
        this.preventKeyBoard = preventKeyBoard;
    }

}
