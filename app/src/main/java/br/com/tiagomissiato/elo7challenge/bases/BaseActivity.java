package br.com.tiagomissiato.elo7challenge.bases;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;

import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.views.TypefaceSpan;


/**
 * Created by tiagomissiato on 7/29/15.
 */
public class BaseActivity extends AppCompatActivity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void setActionBarTitle(String title) {

        ActionBar actionBar = getSupportActionBar();

        if(actionBar != null) {
            if (title != null) {
                SpannableString s = new SpannableString(title);
                s.setSpan(new TypefaceSpan(this, getString(R.string.text_font)), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                actionBar.setTitle(s);
            } else {
                actionBar.setTitle("");
            }
        }
    }
}
