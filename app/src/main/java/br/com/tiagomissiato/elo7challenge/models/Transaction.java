package br.com.tiagomissiato.elo7challenge.models;

import java.io.Serializable;

/**
 * Created by tiagomissiato on 8/23/15.
 */
public class Transaction implements Serializable{

    public String description;
    public String accountFrom;
    public String accountTo;
    public int taxType;
    public int value;
    public int taxValue;
    public long schedule;
    public long createdAt;
    public boolean shared;

    @Override
    public String toString() {

        return super.toString();
    }
}
