package br.com.tiagomissiato.elo7challenge;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import br.com.tiagomissiato.elo7challenge.bases.BaseActivity;
import br.com.tiagomissiato.elo7challenge.helpers.PreferenceHelper;
import br.com.tiagomissiato.elo7challenge.helpers.TwitterHelper;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.logged_twitter)
    SwitchCompat switchTwitter;
    @Bind(R.id.transaction)
    RelativeLayout clearTransaction;
    @Bind(R.id.login_button)
    TwitterLoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        //Set up toolbar
        configureToolbar();

        if(TwitterHelper.getInstance(this).isLogged()){
            loginButton.setVisibility(View.GONE);
            switchTwitter.setVisibility(View.VISIBLE);
            switchTwitter.setChecked(true);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            switchTwitter.setVisibility(View.GONE);
            switchTwitter.setChecked(false);
        }

        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterAuthToken authToken = result.data.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;

                PreferenceHelper.storeTwitterToken(SettingsActivity.this, token, secret);
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });

        switchTwitter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!isChecked){
                    TwitterHelper.getInstance(SettingsActivity.this).logout();
                    loginButton.setVisibility(View.VISIBLE);
                    switchTwitter.setVisibility(View.GONE);
                } else {
                    loginButton.setVisibility(View.GONE);
                    switchTwitter.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        setActionBarTitle(getString(R.string.activity_settings));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginButton.onActivityResult(requestCode, resultCode, data);

        if(TwitterHelper.getInstance(this).isLogged()){
            loginButton.setVisibility(View.GONE);
            switchTwitter.setVisibility(View.VISIBLE);
            switchTwitter.setChecked(true);
        } else {
            loginButton.setVisibility(View.VISIBLE);
            switchTwitter.setVisibility(View.GONE);
            switchTwitter.setChecked(false);
        }

    }

    @OnClick(R.id.transaction)
    public void showClearTransactionDialog(){
        AlertDialog.Builder clearTransactionsDialog = new AlertDialog.Builder(this);
        clearTransactionsDialog.setTitle(getString(R.string.clear_transaction_title));
        clearTransactionsDialog.setMessage(getString(R.string.clear_transaction_message));
        clearTransactionsDialog.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                PreferenceHelper.clearTransactions(SettingsActivity.this);
            }
        });
        clearTransactionsDialog.setNegativeButton(getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        clearTransactionsDialog.show();
    }

}
