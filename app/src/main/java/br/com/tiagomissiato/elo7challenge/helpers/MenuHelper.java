package br.com.tiagomissiato.elo7challenge.helpers;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

/**
 * Created by tiagomissiato on 8/22/15.
 */
public class MenuHelper {

    public static final int DRAWER_MENU_TRANSACTIONS = 0;

    public static boolean isDrawerOpen(DrawerLayout mDrawerLayout) {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START);
    }
}
