package br.com.tiagomissiato.elo7challenge.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import br.com.tiagomissiato.elo7challenge.R;

/**
 * Created by tiagomissiato on 8/22/15.
 */
public class FontFamilyTextView extends TextView {


    Context context;
    String fontFamily = null;

    public FontFamilyTextView(Context context) {
        super(context);
        this.context = context;

        if(!isInEditMode())
            init();
    }

    public FontFamilyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;

        TypedArray a=context.obtainStyledAttributes(attrs, R.styleable.FontFamilyTextView);
        this.fontFamily = a.getString(R.styleable.FontFamilyTextView_fmtvFontFamily);
        a.recycle();

        if(!isInEditMode())
            init();
    }

    public FontFamilyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray a=context.obtainStyledAttributes(attrs, R.styleable.FontFamilyTextView);
        this.fontFamily = a.getString(R.styleable.FontFamilyTextView_fmtvFontFamily);
        a.recycle();

        if(!isInEditMode())
            init();
    }

    void init() {

        if(this.fontFamily != null){
            try {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/"+this.fontFamily);
                this.setTypeface(myTypeface);
            } catch (Exception e) {}

        }

    }
}
