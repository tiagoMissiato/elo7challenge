package br.com.tiagomissiato.elo7challenge.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.com.tiagomissiato.elo7challenge.R;
import br.com.tiagomissiato.elo7challenge.helpers.CurrencyHelper;
import br.com.tiagomissiato.elo7challenge.helpers.DateHelper;
import br.com.tiagomissiato.elo7challenge.models.Transaction;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tiagomissiato on 7/28/15.
 */
public class TransactionListAdapter extends RecyclerView.Adapter<TransactionListAdapter.ViewHolder> {

    Context mContext;
    private static List<Transaction> items;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        View view;
        @Bind(R.id.item_line)
        View line;
        @Bind(R.id.step)
        View step;
        @Bind(R.id.first_item_divider)
        View divider;
        @Bind(R.id.item_text)
        TextView item;
        @Bind(R.id.item_date_text)
        TextView date;
        @Bind(R.id.item_value_text)
        TextView value;
        @Bind(R.id.item_tax_text)
        TextView tax;
        @Bind(R.id.account_from)
        TextView from;
        @Bind(R.id.account_to)
        TextView to;

        public ViewHolder(View iteView) {
            super(iteView);

            ButterKnife.bind(this, iteView);

            this.view = iteView;

        }
    }

    public TransactionListAdapter(Context mContext, List<Transaction> items) {
        TransactionListAdapter.items = items;
        this.mContext = mContext;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View layout = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_transaction_item, null);

        return new ViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        Transaction item = items.get(i);

        holder.date.setText(DateHelper.getInstance().getDateFromTimestamp(item.schedule, "dd/MM/yyyy"));
        holder.item.setText(item.description);
        holder.value.setText(CurrencyHelper.getInstance().getCurrencyFormatSymbol(item.value));
        holder.tax.setText(mContext.getString(R.string.tax_value_type, CurrencyHelper.getInstance().getCurrencyFormatSymbol(item.taxValue), CurrencyHelper.getInstance().getTaxType(item.taxType)));
        holder.from.setText(item.accountFrom);
        holder.to.setText(item.accountTo);
        holder.divider.setVisibility(View.GONE);

        if (i == 0) {
            holder.divider.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.line.getLayoutParams();
            params.height = (int) mContext.getResources().getDimension(R.dimen.transaction_item_line_half);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

            holder.line.setLayoutParams(params);
            if (getItemCount() == 1) {
                holder.line.setVisibility(View.INVISIBLE);
            }
        } else if (i == getItemCount() - 1) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.line.getLayoutParams();
            params.height = (int) mContext.getResources().getDimension(R.dimen.transaction_item_line_half);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);

            holder.line.setLayoutParams(params);
        } else {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.line.getLayoutParams();
            params.height = (int) mContext.getResources().getDimension(R.dimen.transaction_item_line_height);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);

            holder.line.setLayoutParams(params);
        }

    }
}