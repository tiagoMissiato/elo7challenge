package br.com.tiagomissiato.elo7challenge.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import br.com.tiagomissiato.elo7challenge.R;

/**
 * Created by tiagomissiato on 8/23/15.
 */
public class FontFamilyRadioButton extends RadioButton {

    Context context;
    String fontFamily = null;

    public FontFamilyRadioButton(Context context) {
        super(context);
        this.context = context;

        if(!isInEditMode())
            init();
    }

    public FontFamilyRadioButton(Context context, String fontFamily) {
        super(context);
        this.context = context;
        this.fontFamily = fontFamily;

        if(!isInEditMode())
            init();
    }

    public FontFamilyRadioButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;

        TypedArray a=context.obtainStyledAttributes(attrs, R.styleable.FontFamilyRadioButton);
        this.fontFamily = a.getString(R.styleable.FontFamilyButton_fmbtnFontFamily);
        a.recycle();

        if(!isInEditMode())
            init();
    }

    public FontFamilyRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;

        TypedArray a=context.obtainStyledAttributes(attrs, R.styleable.FontFamilyRadioButton);
        this.fontFamily = a.getString(R.styleable.FontFamilyButton_fmbtnFontFamily);
        a.recycle();

        if(!isInEditMode())
            init();
    }

    void init() {

        if(this.fontFamily != null){
            try {
                Typeface myTypeface = Typeface.createFromAsset(context.getAssets(), this.fontFamily);
                this.setTypeface(myTypeface);
            } catch (Exception e) {}

        }

    }

}
